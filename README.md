## Fifth task: Google maps

**Technologies used:**

1. Google maps
2. Retrofit
3. Hilt
4. Coroutines - Flow
5. MVVM   
6. SOLID, Clean Architecture

<img alt="ImageGalleryDemo" src="https://github.com/aleh-god/fifth-task-maps-learn/blob/master/FifthTaskDemo.gif" />